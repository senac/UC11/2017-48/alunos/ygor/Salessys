package br.com.senac.salesys.model;

import java.util.ArrayList;
import java.util.List;


public class Cliente {







    private String Nome;
    private String cidade;
    private String uf;
    private String profissão;
    private  String nomeempresa;
    private String telefone;
    private String email;
    private String Observacoes;

    public Cliente(){


    }

    public Cliente(String nome, String cidade, String uf, String profissão, String nomeempresa, String telefone, String email, String observacoes) {
        Nome = nome;
        this.cidade = cidade;
        this.uf = uf;
        this.profissão = profissão;
        this.nomeempresa = nomeempresa;
        this.telefone = telefone;
        this.email = email;
        Observacoes = observacoes;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getProfissão() {
        return profissão;
    }

    public void setProfissão(String profissão) {
        this.profissão = profissão;
    }

    public String getNomeempresa() {
        return nomeempresa;
    }

    public void setNomeempresa(String nomeempresa) {
        this.nomeempresa = nomeempresa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservacoes() {
        return Observacoes;
    }

    public void setObservacoes(String observacoes) {
        Observacoes = observacoes;
    }





}
