package br.com.senac.salesys.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import br.com.senac.salesys.ClienteActivity;
import br.com.senac.salesys.MainActivity;
import br.com.senac.salesys.R;
import br.com.senac.salesys.model.Cliente;

public class Main3Activity extends AppCompatActivity {
Cliente cliente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        final Button  btnsalvar  = (Button) findViewById(R.id.cadastrar);
        final TextView txtnomecompleto =  (TextView) findViewById(R.id.nomecompleto);
        final TextView txttelefone = (TextView) findViewById(R.id.telefone);
        final TextView txtprofissao = (TextView) findViewById(R.id.profissao);
        final TextView txtemail = (TextView) findViewById(R.id.email);
        final TextView txtobs = (TextView) findViewById(R.id.observaçoes);
        final Spinner  txtestados = (Spinner) findViewById(R.id.estados);
        final TextView txtnomeempresa= (TextView) findViewById(R.id.nomeempresa);
        final Spinner  txtcidade = (Spinner) findViewById(R.id.cidades);

        btnsalvar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String nome = txtnomecompleto.getText().toString();
                String telefone = txttelefone.getText().toString();
                String profissao = txtprofissao.getText().toString();
                String email = txtemail.getText().toString();
                String obs = txtobs.getText().toString();
                String estados =(String)txtestados.getSelectedItem();
                String empresa = txtnomeempresa.getText().toString();
                String cidade = (String)txtcidade.getSelectedItem();


                Cliente cliente = new Cliente(nome,telefone,profissao,email,obs,estados,empresa,cidade);


                 // Toast.makeText(ClienteActivity.this, "salvo com sucesso" ,Toast.LENGTH_LONG).show();
            }
        });

    }
}
